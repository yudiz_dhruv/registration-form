import React, { Component } from 'react'

class Form extends Component {
  constructor () {
    super()
    this.state = {
      fname: '',
      lname: '',
      dob: '',
      age: '',
      occupation: '',
      company: '',
      email: '',
      contact: '',
      isRegistered: []
    }

    // binding onchange event otherwise we won't get the data after being called
    this.handleChange = this.handleChange.bind(this)
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
  }
  //   const [details, setDetails] = useState([])

  handleChange (e) {
    const name = e.target.name
    const value = e.target.value

    this.setState({
      [name]: value
    })
  }

  handleFormSubmit (e) {
    const { fname, lname, dob, age, occupation, email, company, contact } = this.state
    const record = this.state.isRegistered

    record.push({ fname, lname, dob, age, occupation, email, company, contact })
    e.preventDefault()

    this.setState({
      fname: '',
      lname: '',
      dob: '',
      age: '',
      occupation: '',
      company: '',
      email: '',
      contact: '',
      isRegistered: record
    })
  }

  render () {
    return (
        <>
        <div className='form container shadow-lg'>
            <h2>Registration Form</h2><span className='underline'><span className='underline-2'></span></span>

            <form className='mt-5' onSubmit={this.handleFormSubmit}>
                <table className='table table-borderless' cellPadding={8} cellSpacing={8}>
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>
                            <div className="row p-0">
                                <div className="col">
                                    {/* value is compulsary to give otherwise old input will not become empty due to event handling */}
                                    <input type="text" value={this.state.fname} name="fname" id='fname' className="form-control form-control-sm" placeholder="First name" autoComplete="false" onChange={this.handleChange} />
                                </div>
                                <div className="col">
                                    <input type="text" value={this.state.lname} name="lname" id='lname' className="form-control form-control-sm" placeholder="Last name" autoComplete="false" onChange={this.handleChange} />
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Date of Birth</td>
                        <td>
                            <input type='date' value={this.state.dob} name="dob" id='dob' className='form-control form-control-sm' onChange={this.handleChange} />
                        </td>
                    </tr>
                    <tr>
                        <td>Age</td>
                        <td>
                            <input type='number' value={this.state.age} name='age' id='age' className='form-control form-control-sm' placeholder='your age' autoComplete="false" onChange={this.handleChange} />
                        </td>
                    </tr>
                    <tr>
                        <td>Occupation</td>
                        <td>
                            <input type='text' value={this.state.occupation} name='occupation' id='occupation' className='form-control form-control-sm' placeholder='your occupation' autoComplete="false" onChange={this.handleChange} />
                        </td>
                    </tr>
                    <tr>
                        <td>Company</td>
                        <td>
                            <input type='text' value={this.state.company} name='company' id='company' className='form-control form-control-sm' placeholder='your company name' autoComplete="false" onChange={this.handleChange} />
                        </td>
                    </tr>
                    <tr>
                        <td>Email ID</td>
                        <td>
                            <input type='email' value={this.state.email} name='email' id='email' className='form-control form-control-sm' placeholder='eg: example@gmail.com' autoComplete="false" onChange={this.handleChange} />
                        </td>
                    </tr>
                    <tr>
                        <td>Contact No.</td>
                        <td>
                            <input type='number' value={this.state.contact} name='contact' id='contact' className='form-control form-control-sm' placeholder='eg: 9999888777' autoComplete="false" onChange={this.handleChange} />
                        </td>
                    </tr>
                    <tr>
                        <td colSpan="2">
                            <button type='submit' className='btn btn-dark'>Save</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div className='details container shadow-lg mt-5 rounded-3'>
            <h4>Form Details</h4><span className='underline'><span className='underline-2'></span></span>
                <table className='table mt-5' >
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Date of Birth</th>
                            <th>Age</th>
                            <th>Occupation</th>
                            <th>Company</th>
                            <th>Email ID</th>
                            <th>Contact No.</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.isRegistered.map((e, index) => {
                      return (
                        <>
                        { console.log('index', index) }
                        <tr key={e.id}>
                            <td>{e.fname} {e.lname}</td>
                            <td>{e.dob}</td>
                            <td>{e.age}</td>
                            <td>{e.occupation}</td>
                            <td>{e.company}</td>
                            <td>{e.email}</td>
                            <td>{e.contact}</td>
                        </tr>
                    </>
                      )
                    })}
                    </tbody>
            </table>
            <div>

            </div>
        </div>
    </>
    )
  }
}

export default Form
